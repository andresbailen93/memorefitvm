package es.andresbailen.samples.bemorefit.data.datalayer

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import es.andresbailen.samples.bemorefit.view.BeMoreFitMV

class SharedPreferencesManager {

    companion object {
        var context: Context = BeMoreFitMV.instance!!.applicationContext
        var sharedPrefences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        var sharedPrefencesEditor: SharedPreferences.Editor = sharedPrefences.edit()
        var gson: Gson = Gson()

    }

    fun <C> getObject(key: String, classz: Class<C>): C? {
        val json = sharedPrefences.getString(key, null)
        if (json != null) {
            try {
                return gson.fromJson<C>(json, classz)
            } catch (exception: Exception) {
                exception.printStackTrace()
            }

        }
        return null
    }

    fun saveObject(key: String, objt: Any?) {
        if (objt == null)
            return
        saveString(key, gson.toJson(objt))
    }

    //region Boolean Manipulation
    fun getBoolean(key: String): Boolean {
        return getBoolean(key, false)
    }

    fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return sharedPrefences.getBoolean(key, defaultValue)
    }

    fun getLong(key: String, defaultValue: Long): Long {
        return sharedPrefences.getLong(key, defaultValue)
    }

    fun getLong(key: String): Long {
        return sharedPrefences.getLong(key, 0)
    }

    fun saveBoolean(key: String, value: Boolean) {
        sharedPrefencesEditor.putBoolean(key, value).commit()
    }
    //endregion

    //region String Manipulation
    fun getString(key: String): String? {
        return getString(key, null)
    }

    fun getString(key: String, defaultValue: String?): String? {
        return sharedPrefences.getString(key, defaultValue)
    }

    fun saveString(key: String, value: String) {
        sharedPrefencesEditor.putString(key, value).commit()
    }

    fun getInt(key: String, defaultVal: Int): Int {
        return sharedPrefences.getInt(key, defaultVal)
    }

    fun saveInt(key: String, value: Int) {
        sharedPrefencesEditor.putInt(key, value).commit()
    }

    operator fun contains(key: String): Boolean {
        return sharedPrefences.contains(key)
    }
    //endregion

    //region Data Removal
    fun removeEntry(key: String) {
        sharedPrefencesEditor.remove(key).commit()
    }

    fun clearSharedPreferences() {
        sharedPrefences.edit().clear().commit()
    }
    //endregion


}