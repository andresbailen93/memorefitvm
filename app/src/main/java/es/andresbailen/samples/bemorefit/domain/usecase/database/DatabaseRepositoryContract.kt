package es.andresbailen.samples.bemorefit.domain.usecase.database

interface DatabaseRepositoryContract {

    fun initializateDB()

}