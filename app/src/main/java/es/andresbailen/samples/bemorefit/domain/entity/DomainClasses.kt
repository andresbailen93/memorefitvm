package es.andresbailen.samples.bemorefit.domain.entity

data class ForecastList(val city: String, val country: String,
                        val dailyforecast: List<Forecast>){
    operator fun get(position: Int): Forecast = dailyforecast[position]
    fun size(): Int = dailyforecast.size
}

data class Forecast(val date: String, val description: String, val high: Int, val low: Int,
                    val iconUrl: String)