package es.andresbailen.samples.bemorefit.data.repository.contacts


import android.content.Context
import android.provider.ContactsContract
import es.andresbailen.samples.bemorefit.domain.usecase.contacts.ContactsRepositoryContract
import es.andresbailen.samples.bemorefit.data.database.dao.contact.Contact
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe



class ContactsRepository: ContactsRepositoryContract {

    override fun retrieveAllContacts(context:Context): Observable<List<Contact>> {
        return Observable.create<List<Contact>> {emitter ->
            try{
                var mProjection = arrayOf(
                    ContactsContract.Profile._ID,
                    ContactsContract.Profile.DISPLAY_NAME_PRIMARY,
                    ContactsContract.Profile.DISPLAY_NAME
                    //ContactsContract.CommonDataKinds.Phone.NUMBER
                )

                var mProfileCursor = context.contentResolver.query(
                        ContactsContract.Profile.CONTENT_URI,
                        mProjection,
                        null,
                        null,
                        null
                )

                var contactList = mutableListOf<Contact>()
                /*while(mProfileCursor.moveToNext()) {
                    contactList.add(Contact(
                            username =  "",
                            number = 123,
                            surname = ""
                            ))
                }*/



                emitter.onNext(contactList)
            } catch (e: java.lang.Exception){
                e.printStackTrace()
                emitter.onError(e)
                e.printStackTrace()
            }
        }
        }

    }