package es.andresbailen.samples.bemorefit.domain.usecase.login

import es.andresbailen.samples.bemorefit.data.model.LoggedUser
import es.andresbailen.samples.bemorefit.data.repository.login.ManualLoginRepository
import es.andresbailen.samples.bemorefit.domain.usecase.BaseUseCase

class ManualLoginUseCase: BaseUseCase() {

    private var manualLoginRepository: ManualLoginRepositoryContract = ManualLoginRepository()

   // fun doManualLogin(loginUserSubscriber: BaseUseCase.Subscriber<LoggedUser>, manualLoginUserUI: ManualLoginUserUI){
   //     createSubscription(manualLoginRepository.doManualLogin(manualLoginUserUI), loginUserSubscriber)
   // }

    fun saveUserInSharedPreferences(logedUser: LoggedUser){
        manualLoginRepository.saveUserInSharedPreferences(logedUser)
    }

    fun returnIfIsAnyUserInSharedPrefs(): LoggedUser? {
       return manualLoginRepository.checkIfUserIsLogged()
    }

}