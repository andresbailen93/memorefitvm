package es.andresbailen.samples.bemorefit.view.fragment.login

import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import es.andresbailen.samples.bemorefit.R
import es.andresbailen.samples.bemorefit.view.activity.login.LoginActivity
import es.andresbailen.samples.bemorefit.view.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*

class LoginFragment: BaseFragment(), LoginView {


    lateinit var mView: View

    companion object {
        fun newInstance(): LoginFragment = LoginFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_login, container, false)

        mView.btn_cancel.setOnClickListener { /*dismiss()*/ }
        mView.btn_login.setOnClickListener {
            if(checkFields()){
                mView.loading_progress.visibility = View.VISIBLE
                /*loginFragmentPresenter.doManualLogin(
                        ManualLoginUserUI(txtin_email.text.toString(),  txtin_pass.text.toString(), getImei(), txtin_number.text.toString())
                )*/
            }
        }

        return mView
    }

    override fun onResume() {
        super.onResume()
    }

    override fun checkFields(): Boolean {
        if (txtin_email.text?.toString().equals("")){
            txtin_email.error = getString(R.string.field_error)
            txtin_email.requestFocus()
            return false
        }
        if(txtin_pass.text?.toString().equals("")){
            txtin_pass.error = getString(R.string.field_error)
            txtin_pass.requestFocus()
            return false
        }

        if(txtin_number.text?.toString().equals("")){
            txtin_number.error = getString(R.string.field_error)
            txtin_number.requestFocus()
            return false
        }

        return true
    }


    fun getImei(): String{
        return Settings.Secure.getString(this.activity?.applicationContext?.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    override fun hideLoading(){
        loading_progress.visibility = View.GONE
    }

    override fun redirect2Main(){
        (this.activity as LoginActivity).navigateToMain()
    }


}