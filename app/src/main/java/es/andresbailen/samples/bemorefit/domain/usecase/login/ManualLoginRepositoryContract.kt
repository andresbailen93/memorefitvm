package es.andresbailen.samples.bemorefit.domain.usecase.login

import es.andresbailen.samples.bemorefit.data.model.LoggedUser
import io.reactivex.Observable

interface ManualLoginRepositoryContract {

    //fun doManualLogin(manualLoginUserUI: ManualLoginUserUI): Observable<LoggedUser>

    fun saveUserInSharedPreferences(loggedUser: LoggedUser)

    fun checkIfUserIsLogged(): LoggedUser?
}