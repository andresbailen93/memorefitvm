package es.andresbailen.samples.bemorefit.data.database.dao.conversation

import androidx.room.*
import es.andresbailen.samples.bemorefit.data.database.dao.conversation.Conversation

@Dao
interface ConversationDao{
    @Query("SELECT * FROM conversation")
    fun getAll(): List<Conversation>

    @Query("SELECT * FROM conversation WHERE receiver = :uid")
    fun getConversationByReceiver(uid: String): Conversation

    @Insert
    fun insertConversation(consersation: Conversation)

    @Delete
    fun delete(conversation: Conversation)

    @Update
    fun updateConversation(conversation : Conversation)
}