package es.andresbailen.samples.bemorefit.data.database.dao.user

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity()
data class User (
        @PrimaryKey var uid: Int,
        @ColumnInfo var name: String,
        @ColumnInfo var surname: String,
        @ColumnInfo var number: Int
)