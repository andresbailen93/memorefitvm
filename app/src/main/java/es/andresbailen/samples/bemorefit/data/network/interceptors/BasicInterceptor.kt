package es.andresbailen.samples.bemorefit.data.network.interceptors

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class BasicInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var originalRequest = chain.request()

        var builder: Request.Builder = originalRequest.newBuilder().header("Content-Type",
                "application/json")

        var newRequest: Request = builder.build()
        return chain.proceed(newRequest)
    }
}