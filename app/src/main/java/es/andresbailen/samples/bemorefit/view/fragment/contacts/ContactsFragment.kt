package es.andresbailen.samples.bemorefit.view.fragment.contacts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import es.andresbailen.samples.bemorefit.R
import es.andresbailen.samples.bemorefit.view.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_contacts.view.*

class ContactsFragment: BaseFragment(), ContactsView {

    lateinit var mView: View

    companion object {
        fun newInstance(): ContactsFragment = ContactsFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_contacts, container, false)

        inflateLayout()
        return mView
    }

    fun inflateLayout() {

        mView.btn_banner.setOnClickListener {
           // val customDialog: CustomDialog  = CustomDialog(this.requireContext())
           // customDialog.show()

        }
    }

    override fun onResume() {
        super.onResume()
    }
}