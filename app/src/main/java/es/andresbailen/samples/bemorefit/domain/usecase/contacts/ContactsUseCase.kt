package es.andresbailen.samples.bemorefit.domain.usecase.contacts

import es.andresbailen.samples.bemorefit.data.database.dao.contact.Contact
import es.andresbailen.samples.bemorefit.data.repository.contacts.ContactsRepository
import es.andresbailen.samples.bemorefit.domain.usecase.BaseUseCase

class ContactsUseCase: BaseUseCase() {

    var contactsRepository: ContactsRepositoryContract = ContactsRepository()

    fun requestAllContacts (contactListSubscriber: BaseUseCase.Subscriber<List<Contact>>){
        createSubscription(contactsRepository.retrieveAllContacts() , contactListSubscriber)
         //contactsRepository.retrieveAllContacts()

    }
}