package es.andresbailen.samples.bemorefit.data.database.dao.contact

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ContactDao{

    @Query("SELECT * FROM CONTACT ")
    fun getAll():List<Contact>

    @Query("SELECT * FROM CONTACT WHERE uid = :id")
    fun getContact(id: String): Contact

    @Insert
    fun insertAll(vararg contacts: Contact)

    @Delete
    fun deleteContact(contact: Contact)

}