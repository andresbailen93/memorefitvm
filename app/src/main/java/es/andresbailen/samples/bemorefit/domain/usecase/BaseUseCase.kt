package es.andresbailen.samples.bemorefit.domain.usecase

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


open class BaseUseCase {

    private var mDisposable: Disposable? = null

    fun <T> createSubscription(observable: Observable<T>, subscriber: Subscriber<T>) {
        //cancelRequest()
        this.mDisposable = observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(subscriber)
    }

    fun cancelRequest() {
        if (mDisposable != null && !mDisposable!!.isDisposed()) {
            mDisposable!!.dispose()
        }
    }
    

    open class Subscriber<T> : DisposableObserver<T>() {
        override fun onNext(t: T) {

        }

        override fun onError(e: Throwable) {

        }

        override fun onComplete() {

        }

    }

}