package es.andresbailen.samples.bemorefit.data.repository.login

import es.andresbailen.samples.bemorefit.data.LOGGED_USER_SHARED_PREFERENCES
import es.andresbailen.samples.bemorefit.data.datalayer.SharedPreferencesManager
import es.andresbailen.samples.bemorefit.data.model.LoggedUser
import es.andresbailen.samples.bemorefit.data.network.interceptors.BasicInterceptor
import es.andresbailen.samples.bemorefit.domain.usecase.login.ManualLoginRepositoryContract
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ManualLoginRepository : ManualLoginRepositoryContract {
    val BASE_URL = "http://10.0.2.2:3000"
    var retrofit: Retrofit

    init{

        var okHttpClient: OkHttpClient = OkHttpClient().newBuilder().addInterceptor(
                BasicInterceptor()
        ).build()

        retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    /*override fun doManualLogin(manualLoginUserUI: ManualLoginUserUI): Observable<LoggedUser> {
        var loginApi: LoginApi = retrofit.create(LoginApi::class.java)
        return loginApi.registry(LoginUserDataMapper().convertUIModeltoToLoggedUser(manualLoginUserUI))
    }*/

    override fun saveUserInSharedPreferences(loggedUser: LoggedUser) {
        SharedPreferencesManager().saveObject(LOGGED_USER_SHARED_PREFERENCES, loggedUser)
    }

    override fun checkIfUserIsLogged() : LoggedUser? {
        return SharedPreferencesManager().getObject(LOGGED_USER_SHARED_PREFERENCES, LoggedUser::class.java)
    }


}