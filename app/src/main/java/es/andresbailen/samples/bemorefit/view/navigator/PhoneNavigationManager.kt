package es.andresbailen.samples.bemorefit.view.navigator

import android.content.Intent
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import es.andresbailen.samples.bemorefit.view.fragment.calendar.CalendarFragment
import es.andresbailen.samples.bemorefit.view.fragment.chat.ChatFragment
import es.andresbailen.samples.bemorefit.view.fragment.contacts.ContactsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import es.andresbailen.samples.bemorefit.R
import es.andresbailen.samples.bemorefit.view.activity.login.LoginActivity
import es.andresbailen.samples.bemorefit.view.activity.main.MainActivity

object PhoneNavigationManager : BottomNavigationView.OnNavigationItemSelectedListener {
    var currentActivity: AppCompatActivity? = null


    init{
        Log.d("PhoneNavigation", "Inicializo Navigation")
    }

    fun navigateToLogin() {
        val intent = Intent(currentActivity, LoginActivity::class.java)
        startActivity(intent)
    }

    fun navigateToMain() {
        startActivity(Intent(currentActivity, MainActivity::class.java))
    }

    fun startActivity( intent: Intent){
        currentActivity?.startActivity(intent)
    }

    fun openChatFragment(){
        openFragment(ChatFragment.newInstance())
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        when(item.itemId){
            R.id.action_chat -> {
                openFragment(ChatFragment.newInstance())

            }

            R.id.action_contacts -> {
                openFragment(ContactsFragment.newInstance())

            }

            R.id.action_calendar -> {
                openFragment(CalendarFragment.newInstance())

            }
        }

        return true
    }

    private fun openFragment(fragment: Fragment, idContainer: Int = R.id.main_fragmeLayout){
        val transaction = currentActivity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(idContainer, fragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }
}