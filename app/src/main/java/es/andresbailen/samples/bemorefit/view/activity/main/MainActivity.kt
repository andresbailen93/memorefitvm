package es.andresbailen.samples.bemorefit.view.activity.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import es.andresbailen.samples.bemorefit.R
import es.andresbailen.samples.bemorefit.view.fragment.main.MainFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow()
        }
    }

}
