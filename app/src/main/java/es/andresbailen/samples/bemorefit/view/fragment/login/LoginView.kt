package es.andresbailen.samples.bemorefit.view.fragment.login

import es.andresbailen.samples.bemorefit.view.fragment.BaseView

interface LoginView: BaseView {
    fun checkFields() : Boolean

    fun hideLoading()

    fun redirect2Main()
}