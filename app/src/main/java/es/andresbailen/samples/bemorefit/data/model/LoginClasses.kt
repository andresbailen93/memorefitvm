package es.andresbailen.samples.bemorefit.data.model

data class LoggedUser(val _id: String, val name: String, val surname: String, var imei: String, var number: Int )

data class ToLoggedUser(val name: String, val surname: String, var imei: String, var number: String)