package es.andresbailen.samples.bemorefit.view.fragment.calendar

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import es.andresbailen.samples.bemorefit.R
import es.andresbailen.samples.bemorefit.view.fragment.BaseFragment

class CalendarFragment: BaseFragment(), CalendarView {

    lateinit var mView: View

    companion object {
        fun newInstance(): CalendarFragment = CalendarFragment()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_calendar, container, false)
        Log.d(this::class.java.name, "OnCreateView" )
        return mView
    }

    override fun onResume() {
        super.onResume()
        Log.d(this::class.java.name, "OnResume" )
    }
}