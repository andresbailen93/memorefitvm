package es.andresbailen.samples.bemorefit.view.activity.login

import android.os.Bundle
import es.andresbailen.samples.bemorefit.R
import es.andresbailen.samples.bemorefit.view.activity.BaseActivity
import es.andresbailen.samples.bemorefit.view.fragment.login.LoginFragment
import es.andresbailen.samples.bemorefit.view.navigator.PhoneNavigationManager
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity: BaseActivity() {

    var phoneNavigationManager: PhoneNavigationManager = PhoneNavigationManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        phoneNavigationManager.currentActivity = this
        supportFragmentManager.beginTransaction().add(main_fragment.id, LoginFragment()).commit()

    }


    fun navigateToMain() {
        phoneNavigationManager.navigateToMain()
    }
}

