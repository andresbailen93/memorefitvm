package es.andresbailen.samples.bemorefit.view.fragment.chat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import es.andresbailen.samples.bemorefit.R
import es.andresbailen.samples.bemorefit.view.fragment.BaseFragment

class ChatFragment: BaseFragment(), ChatView {
    lateinit var mView: View

    companion object {
        fun newInstance(): ChatFragment = ChatFragment()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_chat, container, false)

        return mView

    }

    override fun onResume() {
        super.onResume()
    }
}