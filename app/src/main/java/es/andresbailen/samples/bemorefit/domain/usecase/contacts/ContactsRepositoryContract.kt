package es.andresbailen.samples.bemorefit.domain.usecase.contacts

import android.content.Context
import es.andresbailen.samples.bemorefit.data.database.dao.contact.Contact
import es.andresbailen.samples.bemorefit.view.BeMoreFitMV
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe


interface ContactsRepositoryContract {

    fun retrieveAllContacts(context: Context = BeMoreFitMV.instance().applicationContext): Observable<List<Contact>>

}