package es.andresbailen.samples.bemorefit.view.activity.splash

import android.os.Bundle
import es.andresbailen.samples.bemorefit.R
import es.andresbailen.samples.bemorefit.view.activity.BaseActivity
import es.andresbailen.samples.bemorefit.view.navigator.PhoneNavigationManager
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity: BaseActivity() {

    var phoneNavigationManager = PhoneNavigationManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        phoneNavigationManager.currentActivity = this
        fab.setOnClickListener { redirectToLogin() }
    }

    override fun onResume() {
        super.onResume()
    }

    fun redirectToLogin(){
        phoneNavigationManager.navigateToLogin()
    }
}