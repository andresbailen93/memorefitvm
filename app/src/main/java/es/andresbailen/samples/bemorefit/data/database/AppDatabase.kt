package es.andresbailen.samples.bemorefit.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import es.andresbailen.samples.bemorefit.data.database.dao.contact.Contact
import es.andresbailen.samples.bemorefit.data.database.dao.contact.ContactDao
import es.andresbailen.samples.bemorefit.data.database.dao.conversation.Conversation
import es.andresbailen.samples.bemorefit.data.database.dao.conversation.ConversationDao
import es.andresbailen.samples.bemorefit.data.database.dao.user.User
import es.andresbailen.samples.bemorefit.data.database.dao.user.UserDao
import es.andresbailen.samples.bemorefit.view.BeMoreFitMV

@Database(entities = arrayOf(User::class, Contact::class, Conversation::class), version = 0)
abstract  class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun contactDao(): ContactDao
    abstract fun conversationDao(): ConversationDao

    companion object {

        @Volatile private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context = BeMoreFitMV.instance!!.applicationContext): AppDatabase =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
                }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context, AppDatabase::class.java, "database-v0").build()
    }
}