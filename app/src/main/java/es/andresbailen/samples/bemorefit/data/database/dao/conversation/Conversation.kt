package es.andresbailen.samples.bemorefit.data.database.dao.conversation

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

//TODO: Mirar FTS4 en release ROOM v2.1.0 alpha2
//@Fts4
@Entity
data class Conversation(
        @PrimaryKey var uid: Int,
        //TODO: ¿Debemos hacer referencia a usuarios en si?
        @ColumnInfo(name= "receiver") var user: String,
        @ColumnInfo(name = "conversation") var conversation: String

)