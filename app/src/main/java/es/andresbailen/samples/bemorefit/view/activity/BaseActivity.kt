package es.andresbailen.samples.bemorefit.view.activity

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import es.andresbailen.samples.bemorefit.R

open class BaseActivity: AppCompatActivity() {

    lateinit var mainItemDrawerList: List<DrawerMenuItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainItemDrawerList = StringArraytoMenuItemMapper().mainMenuItems(this)

    }
}

data class DrawerMenuItem(var id: String?,var parentId: String?, var title: String, var category: String?, var accountType: String?) {

}

class StringArraytoMenuItemMapper {

    open fun mainMenuItems(context: Context) : List<DrawerMenuItem>{
        var items = mutableListOf<DrawerMenuItem>()
        var itemsString: Array<String> = context.resources.getStringArray(R.array.action_main_drawer)
        items.add(DrawerMenuItem("0", "0",itemsString[0], "0", "main" ))
        items.add(DrawerMenuItem("1", "0",itemsString[1], "0", "main" ))
        items.add(DrawerMenuItem("2", "0",itemsString[2], "0", "main" ))
        items.add(DrawerMenuItem("3", "0",itemsString[3], "0", "main" ))
        /* for(item in itemsString){
             items.add(DrawerMenuItem( null, null, item, null ,null))
     }*/
        return items
    }

}