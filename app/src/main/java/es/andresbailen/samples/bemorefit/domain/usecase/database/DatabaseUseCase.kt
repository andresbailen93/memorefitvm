package es.andresbailen.samples.bemorefit.domain.usecase.database

import es.andresbailen.samples.bemorefit.data.repository.database.DatabaseRepository

class DatabaseUseCase {

    var databaseRepository: DatabaseRepositoryContract = DatabaseRepository()

    fun initDB(){
        databaseRepository.initializateDB()
    }
}