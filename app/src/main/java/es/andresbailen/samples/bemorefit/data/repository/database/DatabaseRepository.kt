package es.andresbailen.samples.bemorefit.data.repository.database

import es.andresbailen.samples.bemorefit.data.database.AppDatabase
import es.andresbailen.samples.bemorefit.domain.usecase.database.DatabaseRepositoryContract

class DatabaseRepository: DatabaseRepositoryContract {

    override fun initializateDB() {
        AppDatabase.getInstance()
    }
}