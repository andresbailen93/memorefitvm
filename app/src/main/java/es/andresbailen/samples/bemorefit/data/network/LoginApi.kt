package es.andresbailen.samples.bemorefit.data.network
import es.andresbailen.samples.bemorefit.data.model.LoggedUser
import es.andresbailen.samples.bemorefit.data.model.ToLoggedUser
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST


interface LoginApi {

    @POST("/registry")
    fun registry( @Body toLogedUser: ToLoggedUser): Observable<LoggedUser>
}