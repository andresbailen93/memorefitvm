package es.andresbailen.samples.bemorefit.view

import android.app.Application
import es.andresbailen.samples.bemorefit.domain.usecase.database.DatabaseUseCase

class BeMoreFitMV : Application(){

    companion object {
        var instance: Application? = null
        fun instance() = instance!!
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        //DatabaseUseCase().initDB()
    }
}